package com.kii.sample.simple_jpush;

import cn.jpush.android.api.JPushInterface;
import android.app.NotificationManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;

public class JPushMessageReceiver extends BroadcastReceiver {

    private static final String TAG = "JPushMessageReceiver";
    private NotificationManager manager;

    @Override
    public void onReceive(Context context, Intent intent) {
        if (manager == null) {
            manager = (NotificationManager) context
                    .getSystemService(Context.NOTIFICATION_SERVICE);
        }
        Bundle bundle = intent.getExtras();
        Log.d(TAG, "onReceive is called - " + intent.getAction() + ", extras: "
                + showBundle(bundle));
    }

    private static String showBundle(Bundle bundle) {
        StringBuilder sb = new StringBuilder();
        for (String key : bundle.keySet()) {
            if (key.equals(JPushInterface.EXTRA_NOTIFICATION_ID)) {
                sb.append("\n[key] :" + key + ", [value] :" + bundle.getInt(key));
            } else {
                sb.append("\n[key] :" + key + ", [value] :" + bundle.getString(key));
            }
        }
        return sb.toString();
    }
}
