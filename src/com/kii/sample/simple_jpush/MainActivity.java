package com.kii.sample.simple_jpush;

import cn.jpush.android.api.JPushInterface;
import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;
import android.widget.TextView;

public class MainActivity extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        // Set debug mode
        JPushInterface.setDebugMode(true);
        // Set alias
        JPushInterface.setAlias(this, "PushAlias", null);
        // Do initialize
        JPushInterface.init(this);

        // Show JPushInterface.getUdid value
        TextView udidView = (TextView) findViewById(R.id.udidView);
        udidView.setText(JPushInterface.getUdid(this));
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.main, menu);
        return true;
    }

}
